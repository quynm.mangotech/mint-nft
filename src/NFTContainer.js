import React from "react";
import NFTCard from "./NFTCard";


const NFTContrainer = ({nfts}) => {
    return (
        <div>
            {nfts.map((nft, index) => {
                return <NFTCard nft={nft} key={index} />
            })}
        </div>
    )
}

export default NFTContrainer