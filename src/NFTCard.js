import React from "react";
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

const NFTCard = ({nft}) => {
    return (
    <Card  style={{ width: '20rem', display: "inline-block", marginRight: "2rem", marginBottom: "2rem" }}>
      <Card.Img variant="top" src="https://tinhhoabacbo.com/wp-content/uploads/2021/12/cute-pho-mai-que.jpg" />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the
          bulk of the card's content.
        </Card.Text>
        <Button variant="success">STAKE NFT</Button>
      </Card.Body>
    </Card>
    )
}

export default NFTCard
