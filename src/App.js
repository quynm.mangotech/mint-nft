import React, { useEffect, useState } from 'react';
import {ethers} from 'ethers';
import axios from "axios";
import {useMoralisWeb3Api } from "react-moralis";
import Moralis from 'moralis';
import NFTContainer from "./NFTContainer";
const serverUrl = "https://n6zqvvytc7u4.usemoralis.com:2053/server"
const appId =  "5LWwZMPUH6UZmfiGMaCe4gsqwKK5TIU99EEstWPM"

function App() {
  const [defaultAccount, setDefaultAccount] = useState('');
  const [userBalance, setUserBalance] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  const [buttonText, setButtonText ] = useState("Connect Wallet");
  const [nftList, setNftList] = useState([])
  const MyNftAddress = "0xF10E6CfF5c601CaD2289D6e9CB113ec73b24ADcd";
  const MyNftAbi = [
    {
      "inputs": [],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "owner",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "approved",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "Approval",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "owner",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "operator",
          "type": "address"
        },
        {
          "indexed": false,
          "internalType": "bool",
          "name": "approved",
          "type": "bool"
        }
      ],
      "name": "ApprovalForAll",
      "type": "event"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": true,
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "indexed": true,
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "Transfer",
      "type": "event"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "approve",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "owner",
          "type": "address"
        }
      ],
      "name": "balanceOf",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_itmeId",
          "type": "uint256"
        },
        {
          "internalType": "string",
          "name": "_tokenURI",
          "type": "string"
        }
      ],
      "name": "buyToken",
      "outputs": [],
      "stateMutability": "payable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "_itemId",
          "type": "uint256"
        }
      ],
      "name": "checkExistence",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "getApproved",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "owner",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "operator",
          "type": "address"
        }
      ],
      "name": "isApprovedForAll",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "name",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "ownerOf",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "safeTransferFrom",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        },
        {
          "internalType": "bytes",
          "name": "_data",
          "type": "bytes"
        }
      ],
      "name": "safeTransferFrom",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "operator",
          "type": "address"
        },
        {
          "internalType": "bool",
          "name": "approved",
          "type": "bool"
        }
      ],
      "name": "setApprovalForAll",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "bytes4",
          "name": "interfaceId",
          "type": "bytes4"
        }
      ],
      "name": "supportsInterface",
      "outputs": [
        {
          "internalType": "bool",
          "name": "",
          "type": "bool"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [],
      "name": "symbol",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "tokenURI",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        }
      ],
      "stateMutability": "view",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "from",
          "type": "address"
        },
        {
          "internalType": "address",
          "name": "to",
          "type": "address"
        },
        {
          "internalType": "uint256",
          "name": "tokenId",
          "type": "uint256"
        }
      ],
      "name": "transferFrom",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    }
  ];

  // useEffect( async() => {
   
  //   // await Moralis.start({serverUrl, appId});
  //   // await fetchNFTsForContract();
  //   // return 1;
  // }, )

  useEffect(() => {
    Moralis.start({serverUrl, appId})
    if(defaultAccount) {
      fetchNFTsForContract(defaultAccount)

    }
  }, [defaultAccount])

  const Web3Api = useMoralisWeb3Api();
  const fetchNFTsForContract = async (address) => {
    const options = {
      chain: "bsc testnet",
      address: address,
      token_address: "0xA3c1CB5cB125B8a123C58Eff6a7e2E0A3fe36063",
    };
    const NFTs = await Web3Api.account.getNFTsForContract(options);
    setNftList(NFTs.result)
    console.log(NFTs);

  };

  async function getMyNftContract() {
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const signer = provider.getSigner();
    const MyNftContract = new ethers.Contract(MyNftAddress, MyNftAbi, signer);
    return MyNftContract;
  }

  if(window.ethereum) {
    window.ethereum.on('accountsChanged', accountChangedHandler);
    window.ethereum.on('chainChanged', chainChangedHandler);
  }


  async function isExisted(number) {
    const MyNft = await getMyNftContract();
    return await MyNft.checkExistence(number)
  }
  
  async function randomNumber() {
    const number = Math.floor(Math.random() * 8) + 1;
    if(await isExisted(number) === false) {
        return number;
    } else {
        await randomNumber();
    }
  }

  async function requestAccount() {
    if (window.ethereum) {
      const accounts = await window.ethereum.request({ method: 'eth_requestAccounts'});
      await accountChangedHandler(accounts);
      setButtonText("Wallet connected");
    } else {
      setErrorMessage("Need to install Metamask");
    }
  }

  async function accountChangedHandler(newAccount) {
    setDefaultAccount(newAccount[0]);
    // console.log(newAccount[0])
    // console.log(defaultAccount)
    await getUserBalance(newAccount.toString())
    
  
  }

  async function getUserBalance(account) {
    if(account) {
      const balance = await window.ethereum.request({ method: 'eth_getBalance', params: [account, 'latest']});
      setUserBalance(ethers.utils.formatEther(balance))
    } else {
      setUserBalance('')
      setButtonText("Connect Wallet")
    }
  }
  

  function chainChangedHandler() {
    window.location.reload();
  }

  async function mintNft() {
    await requestAccount();
    const number =await randomNumber();
    let result = await axios.get('http://localhost:8080/upload-cdn');
    let Uri = `https://gateway.pinata.cloud/ipfs/${result.data.IpfsHash}`;
    const MyNftContract = await getMyNftContract();
    const trans = await MyNftContract.buyToken(number, Uri, {value: ethers.utils.parseEther('0.05')});
    console.log(trans)
  }
 
  return (
    <div className="App">
      <header className="App-header">
        <button style={{display:"block", marginBottom: "10px"}} onClick={requestAccount}>{buttonText}</button>
        <button style={{display:"block", marginBottom: "10px"}} onClick={mintNft} disabled>Mint NFT</button>
        <button style={{display:"block", marginBottom: "10px"}} onClick={fetchNFTsForContract}>Test</button>

        <h3>Wallet address: {defaultAccount}</h3>
        <h3>Blance: {userBalance}</h3>
        {errorMessage}
      </header>
      <NFTContainer nfts={nftList} />
    </div>
  );
}

export default App;
