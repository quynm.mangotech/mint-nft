// const { expect } = require("chai");
// const { ethers } = require("hardhat");

// describe("MyNFT", function() {
//     let myNFT;
//     let owner;
//     let price = '0.05'
//     beforeEach(async() => {
//         [owner] = await ethers.getSigners();
//         const MyNFT = await ethers.getContractFactory("MyNFT");
//         myNFT = await MyNFT.deploy(ethers.utils.parseEther(price));
//         await myNFT.deployed();
//     })
//     it("Should buy an NFT", async function() {
//         await myNFT.buyToken(1,"https://gateway.pinata.cloud/ipfs/QmaLv3xn8AUjmZAfZmPKtYfk9v2xkMcRoYBUap6w4wJ1Sc", {value: ethers.utils.parseEther('0.05')})
//         expect(await myNFT.name()).to.equal('QUYMINH')
//         expect(await myNFT.symbol()).to.equal("QUY")
//         expect(await myNFT.ownerOf(1)).to.equal(owner.address)
//         expect(await myNFT.tokenURI(1)).to.equal("https://gateway.pinata.cloud/ipfs/QmaLv3xn8AUjmZAfZmPKtYfk9v2xkMcRoYBUap6w4wJ1Sc")
//     })
// })