// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract MyNFT is ERC721URIStorage {
    mapping(uint256 => bool) private isExisted;
    address payable owner;
    uint constant price = 50000000000000000;

    constructor() ERC721("QUYMINH", "QUY") {
        owner = payable(msg.sender);
    }

    function mintToken(uint256 _itemId, string memory _tokenURI)
        private
    {
        _safeMint(msg.sender, _itemId);
        _setTokenURI(_itemId, _tokenURI);
        isExisted[_itemId] = true;
        // setApprovalForAll(address(this), true);
    }
    function checkExistence(uint256 _itemId) external view returns(bool) {
        return isExisted[_itemId];
    }

    function buyToken(uint256 _itmeId,string memory _tokenURI) external payable {
        require(msg.value == price, "You must pay the price of the item");
        owner.transfer(msg.value);
        mintToken(_itmeId, _tokenURI);
    }
}